BUILD := ./build
SRC_PATH := ./source
SRC_FILES := $(wildcard $(SRC_PATH)/*.cpp)

$(BUILD)/MySQLLogParser:
	mkdir -p $(dir $@)
	g++ $(SRC_FILES) -o $@ -lmysqlcppconn 