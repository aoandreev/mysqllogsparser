Building: 

	Requirenments:
		- g++ version with C++11 support
		- libmysqlcppconn-dev packet. You can do this with following command:
			sudo apt install libmysqlcppconn-dev
			
	For building an app run Makefile.

Run app with args like:
	'./MySQLLogParser --startDate=2017-01-01.13:00:00 --duration=hourly --threshold=100'
		
################################################################


MySQL Schema for DB with all logs is:
	{
		LogTime   DateTime, 
		IP        Varchar(16), 
		Request   Varchar(32), 
		ReqStatus Varchar(16), 
		UserAgent Varchar(256)
	}
	
MySQL Schema for DB with parsing results is:	
	
	{
		IP        Varchar(16), 
		Comment   Varchar(256)
	}
	
################################################################	
	
SQL test:

1.

DECLARE @start_time DateTime;
SET @start_time = '2017-01-01 13:00:00';
DECLARE @finish_time  DateTime;
SET @finish_time = '2017-01-01 14:00:00';

SELECT T1.IP  FROM
(SELECT IP, COUNT(IP) AS Amount
FROM Logs
WHERE LogTime BETWEEN @start_time AND @finish_time
GROUP BY IP
) T1
WHERE T1.Amount > 30;

2. 
DECLARE @SOME_IP varchar(16);
SET @SOME_IP = '123.123.123.123';
SELECT * FROM Test.Logs WHERE IP = @SOME_IP;
