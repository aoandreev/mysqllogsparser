#pragma once
#include "MySQLManager.h"
#include <iostream>
#include <chrono>
#include <memory>
#include <map>

class LogEntry;
class LogParser
{
public:
	LogParser(
		std::string& _file_path, 
		std::chrono::system_clock::time_point _start_date,
		std::chrono::system_clock::duration _duration,
		int _threshold,
	    std::shared_ptr<MySQLManager> _logs_db,
		std::shared_ptr<MySQLManager> _parse_res_db
	);

	~LogParser();

	bool RunParsing();
	bool WriteToLogDB(LogEntry& _entry);
	bool WriteToParserResDB(const std::string& _ip, const std::string& _comment) ;
	bool AllIsGood() const;

private:
	std::string m_file_path;

	std::chrono::system_clock::time_point m_start_date;
	std::chrono::system_clock::duration   m_duration;
	int                                   m_threshold;

	std::shared_ptr<MySQLManager>         m_logs_db = nullptr;
	std::shared_ptr<MySQLManager>         m_parse_res_db = nullptr;
};

