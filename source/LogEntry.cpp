#include "LogEntry.h"
#include "Defines.h"
#include <sstream>
#include <iomanip>

LogEntry::LogEntry()
{
}

LogEntry::~LogEntry()
{
}

LogEntry::LogEntry(std::string&& _entry) 
{
	this->Parse(std::move(_entry));
}

bool LogEntry::Parse(std::string&& _entry) 
{
	int curr_pos = 0, prev_pos = 0;

	//parse date and time
	if ((curr_pos = _entry.find(LOG_DELIMETER, prev_pos)) == std::string::npos)
		return false;
	
	LogEntry::ConvertFromStrToSysClcTP(_entry.substr(prev_pos, curr_pos - prev_pos), this->m_date_time, "%Y-%m-%d %H:%M:%S");
	prev_pos = curr_pos + 1;

	//parse IP
	if ((curr_pos = _entry.find(LOG_DELIMETER, prev_pos)) == std::string::npos)
		return false;
	this->m_ip = _entry.substr(prev_pos, curr_pos - prev_pos);
	prev_pos = curr_pos + 1;

	//parse request
	if ((curr_pos = _entry.find(LOG_DELIMETER, prev_pos)) == std::string::npos)
		return false;
	this->m_request = _entry.substr(prev_pos, curr_pos - prev_pos);
	prev_pos = curr_pos + 1;

	//parse statu
	if ((curr_pos = _entry.find(LOG_DELIMETER, prev_pos)) == std::string::npos)
		return false;
	this->m_status = _entry.substr(prev_pos, curr_pos - prev_pos);
	prev_pos = curr_pos + 1;

	//parse user_agent
	this->m_user_agent = _entry.substr(prev_pos, _entry.size() - prev_pos);

	return true;
}

std::chrono::system_clock::time_point LogEntry::GetDate_Time() const 
{
	return this->m_date_time;
}

std::string LogEntry::GetIP() const 
{
	return this->m_ip;
}

std::string LogEntry::GetRequest() const
{
	return this->m_request;
}

std::string LogEntry::GetStatus() const
{
	return this->m_status;
}

std::string LogEntry::GetUserAgent() const
{
	return this->m_user_agent;
}

bool LogEntry::ConvertFromStrToSysClcTP(
	const std::string& _str, 
	std::chrono::system_clock::time_point& _tp, 
	std::string&& _format)
{

	std::istringstream iss(_str);
	std::tm tm{};
	if (!(iss >> std::get_time(&tm, _format.c_str())))
		return false;
	_tp = std::chrono::system_clock::from_time_t(std::mktime(&tm));
	return true;
}


bool LogEntry::ConvertFromSysClcTPToStr(
	std::string& _str,
	const std::chrono::system_clock::time_point& _tp,
	std::string&& _format)
{
	std::ostringstream oss;
	std::time_t t_t = std::chrono::system_clock::to_time_t(_tp);
	std::tm t_m {};
	if (!(oss << std::put_time(std::localtime(&t_t), _format.c_str())))
		return false;
	_str = oss.str();
	return true;
}