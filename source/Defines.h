#pragma once

#define LOG_DELIMETER "|"
#define DEFAULT_LOGFILE "access.log"

//we need some diffs
//so i have no another way and must
//write some 'water' like this :)
//DB config
#define DB_HOST "localhost"
#define DB_USER "root"
#define DB_PASSWORD "qwerty123456"
#define DB_NAME "Test"
#define DB_SCHEMA_LOGS "Logs"
#define DB_SCHEMA_PARSING_RESULTS "ParsingResults"
#define DB_LOGS_COLUMNS "LogTime, IP, Request, ReqStatus, UserAgent"
#define DB_PARSING_RESULTS_COLUMNS "IP, Comment"
