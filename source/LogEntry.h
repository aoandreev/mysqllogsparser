#pragma once
#include <iostream>
#include <chrono>

class LogEntry
{
public:
	LogEntry();
	LogEntry(std::string&& _entry); //create entry with parsing entry string
	~LogEntry();

	bool Parse(std::string&& _entry);

	std::chrono::system_clock::time_point GetDate_Time() const;
	std::string GetIP() const;
	std::string GetRequest() const;
	std::string GetStatus() const;
	std::string GetUserAgent() const;

	static bool ConvertFromStrToSysClcTP(const std::string& _str, std::chrono::system_clock::time_point& _tp , std::string&& _format);
	static bool ConvertFromSysClcTPToStr(std::string& _str, const std::chrono::system_clock::time_point& _tp, std::string&& _format);
private:
	std::chrono::system_clock::time_point m_date_time;
	std::string m_ip;
	std::string m_request;
	std::string m_status;
	std::string m_user_agent;
};

