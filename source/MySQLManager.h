#pragma once

/*Works with using MySQL Connector/C++*/

#include <iostream>

namespace sql
{
	class Driver;
	class Connection;
	class Statement;
	class ResultSet;
	class SQLException;
}

class MySQLManager
{
public:
	MySQLManager(std::string _host, std::string _user, std::string _password, std::string _schema);
	~MySQLManager();
	bool Connect();
	bool ExecQuery(std::string _query);
	bool Disconnect();
	void ExatractError(sql::SQLException &e);
private:

	std::string m_host;
	std::string m_user;
	std::string m_password;
	std::string m_schema;

	sql::Driver*     m_driver;
	sql::Connection* m_con;
	sql::Statement*  m_stmt;
	sql::ResultSet*  m_res;
};

