#include "Defines.h"
#include "LogParser.h"
#include "LogEntry.h"
#include <fstream>

LogParser::~LogParser()
{
}


LogParser::LogParser(
	std::string& _file_path,
	std::chrono::system_clock::time_point _start_date,
	std::chrono::system_clock::duration _duration,
	int _threshold,
	std::shared_ptr<MySQLManager> _logs_db,
	std::shared_ptr<MySQLManager> _parse_res_db
) 
	:m_file_path(_file_path)
	,m_start_date(_start_date)
	,m_duration(_duration)
	,m_threshold(_threshold)
	,m_logs_db(_logs_db)
	,m_parse_res_db(_parse_res_db)

{}

bool LogParser::RunParsing()
{
	if (this->AllIsGood())
	{
		std::ifstream log_file(m_file_path);
		if (log_file.is_open())
		{
			
			std::string line;
			LogEntry entry;
			std::map<std::string/*IP*/, int/*counter*/> entries_map;
			uint64_t left_date = m_start_date.time_since_epoch().count();
			uint64_t right_date = (this->m_start_date + this->m_duration).time_since_epoch().count();
			this->m_logs_db->Connect();
			while (!log_file.eof())
			{
				std::getline(log_file, line);
				entry.Parse(std::move(line));
				
				if (this->m_logs_db != nullptr) 
				{
					this->WriteToLogDB(entry);
				}

				if (entry.GetDate_Time().time_since_epoch().count() >= left_date
					&& entry.GetDate_Time().time_since_epoch().count() <= right_date)
				{
					entries_map[entry.GetIP()]++;
				}
			}
			this->m_logs_db->Disconnect();
			log_file.close();

			for (auto && ip : entries_map) 
			{
				if (ip.second > this->m_threshold)
				{
					std::string comment("User with IP = " + ip.first + " made " + std::to_string(ip.second) + " requests during asked time.");
					if (this->m_parse_res_db != nullptr)
					{
						this->WriteToParserResDB(ip.first, comment);
					}
					std::cout << comment << std::endl;
				}
			}
		}
	}
	return false;

}


bool LogParser::AllIsGood() const 
{
	bool res = this->m_start_date.time_since_epoch().count() != 0;
	res &= this->m_duration != std::chrono::hours(0);
	res &= this->m_threshold != 0;
	return res;
}


bool LogParser::WriteToLogDB(LogEntry& _entry)
{
	std::string date_time;
	LogEntry::ConvertFromSysClcTPToStr(date_time, _entry.GetDate_Time(), "%Y-%m-%d %H:%M:%S");
	
	std::string query_string ("INSERT INTO " + std::string(DB_NAME) + "." + std::string(DB_SCHEMA_LOGS) 
		+ " (" + std::string(DB_LOGS_COLUMNS) + ") VALUES (");
	query_string += "'" + date_time + "', ";
	query_string += "'" + _entry.GetIP() + "', ";
	query_string += "'" + _entry.GetRequest() + "', ";
	query_string += "'" + _entry.GetStatus() + "', ";
	query_string += "'" + _entry.GetUserAgent() + "');";

	return this->m_logs_db->ExecQuery(query_string);
}

bool LogParser::WriteToParserResDB(const std::string& _ip, const std::string& _comment)
{
	std::string query_string("INSERT INTO " + std::string(DB_NAME) + "." + std::string(DB_SCHEMA_PARSING_RESULTS)
		+ " (" + std::string(DB_PARSING_RESULTS_COLUMNS) + ") VALUES (");
	query_string += "'" + _ip + "', ";
	query_string += "'" + _comment + "');";

    return this->m_parse_res_db->ExecQuery(query_string);
}