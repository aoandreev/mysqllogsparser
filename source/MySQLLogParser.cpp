#include "Defines.h"
#include "MySQLManager.h"
#include "LogParser.h"
#include "LogEntry.h"
#include <string>

bool RunParsing(int _argc, char** _argv)
{
	std::string str_start_date;
	std::string str_duration;
	std::string str_threshold;
	std::string file_path;

	for (int i = 0; i < _argc; ++i)
	{
		std::string curr_arg(_argv[i]);
		if (curr_arg.find("-startDate") != std::string::npos)
			str_start_date = curr_arg.substr(curr_arg.find("=") + 1);
		else if (curr_arg.find("-duration") != std::string::npos)
			str_duration = curr_arg.substr(curr_arg.find("=") + 1);
		else if (curr_arg.find("-threshold") != std::string::npos)
			str_threshold = curr_arg.substr(curr_arg.find("=") + 1);
	}

	if (str_start_date.empty() || str_duration.empty() || str_threshold.empty())
	{
		std::string missed_arguments = (str_start_date.empty() ? "startDate, " : "");
		missed_arguments += (str_duration.empty() ? "duration, " : "");
		missed_arguments += (str_threshold.empty() ? "threshold " : "");
		std::cerr << "Invalid set of the params. Missed: "
			<< missed_arguments
			<< "Required line like this './MySQLLogParser --startDate=2017-01-01.13:00:00 --duration=hourly --threshold=100'" << std::endl;
		return false;
	}

	//startDate
	std::chrono::system_clock::time_point start_date;
	if (!LogEntry::ConvertFromStrToSysClcTP(str_start_date, start_date, "%Y-%m-%d.%H:%M:%S"))
	{
		std::cerr << "Invalid data-time format in arg -startDate" << std::endl;
		return false;
	}

	//duration
	std::chrono::system_clock::duration duration;
	//"hourly" or "daily"
	if (str_duration == "hourly")
		duration = std::chrono::hours(1);
	else if (str_duration == "daily")
		duration = std::chrono::hours(24);
	else
		duration = std::chrono::hours(0);
	
	//threshold
	int threshold(std::stoi(str_threshold));

	//filepath
	std::cout << "Input log file path. '0' for default file path (access.log in current directory)." << std::endl;
	std::cin >> file_path;
	if (file_path == "0")
	{
		file_path = DEFAULT_LOGFILE;
	}

	auto logs_db      = std::make_shared<MySQLManager>(DB_HOST, DB_USER, DB_PASSWORD, DB_SCHEMA_LOGS);
	auto parse_res_db = std::make_shared<MySQLManager>(DB_HOST, DB_USER, DB_PASSWORD, DB_SCHEMA_PARSING_RESULTS);

	LogParser log_parser(file_path, start_date, duration, threshold, logs_db, parse_res_db);
	
	log_parser.RunParsing();

	return true;
}



int main(int argc, char** argv)
{
	//./MySQLLogParser --startDate=2017-01-01.13:00:00 --duration=hourly --threshold=100
	RunParsing(argc, argv);
    return 0;
}

