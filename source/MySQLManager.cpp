#include "MySQLManager.h"
#include <mysql_connection.h>
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>

MySQLManager::MySQLManager(std::string _host, std::string _user, std::string _password, std::string _schema)
	:m_host(_host)
	,m_user(_user)
	,m_password(_password)
	,m_schema(_schema)
{
}

MySQLManager::~MySQLManager()
{
}

bool MySQLManager::Connect() 
{
	try {
		m_driver = get_driver_instance();
		m_con = m_driver->connect(m_host.c_str(), m_user.c_str(), m_password.c_str());
		m_con->setSchema(m_schema.c_str());
		m_stmt = m_con->createStatement();
	}
	catch (sql::SQLException &e) {
		std::cerr << "MySQL Connection error!" << std::endl ;

		this->ExatractError(e);
		return false;
	}
	return true;
}

bool MySQLManager::ExecQuery(std::string _query) 
{
	try {
		m_res = m_stmt->executeQuery(_query.c_str());
	}
	catch (sql::SQLException &e) {
		std::cerr << "MySQL ExecQuery error!" << std::endl;

		this->ExatractError(e);
		return false;
	}
	return true;
}

bool MySQLManager::Disconnect() 
{	
	try {
		delete m_res;
		delete m_stmt;
		delete m_con;
	}
	catch (sql::SQLException &e) {
		std::cerr << "MySQL Disconnect error!" << std::endl;
		this->ExatractError(e);
		return false;
	}
	return true;
}

void MySQLManager::ExatractError(sql::SQLException &e)
{
	std::cerr << "# ERR: " << e.what();
	std::cerr << " (MySQL error code: " << e.getErrorCode();
	std::cerr << ", SQLState: " << e.getSQLState() << " )" << std::endl;
}
